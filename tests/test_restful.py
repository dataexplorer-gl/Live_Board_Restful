import os
import live_order_restful
import unittest
import json


def add_data(app, header):
    entry1 = dict(userid=111, quantity=2, price=2.3, type="BUY")
    rv = app.post("/register_order", data=json.dumps(entry1), headers=header)
    assert rv.status_code == 200
    entry2 = dict(userid=111, quantity=2, price=1.2, type="SELL")
    rv = app.post("/register_order", data=json.dumps(entry2), headers=header)
    assert rv.status_code == 200
    entry3 = dict(userid=111, quantity=20, price=1.3, type="SELL")
    rv = app.post("/register_order", data=json.dumps(entry3), headers=header)
    assert rv.status_code == 200
    entry4 = dict(userid=113, quantity=20, price=1.3, type="SELL")
    rv = app.post("/register_order", data=json.dumps(entry4), headers=header)
    assert rv.status_code == 200

class FlaskrTestCase(unittest.TestCase):

    header = {"Content-Type": "application/json"}

    def setUp(self):
        live_order_restful.app.config['TESTING'] = True
        self.app = live_order_restful.app.test_client()

    def tearDown(self):
        pass

    def test_register_order(self):
        add_data(self.app, self.header)

    def test_delete_order(self):
        add_data(self.app, self.header)
        non_existent_entry = dict(userid=112, quantity=2, price=2.3, type="BUY")
        rv = self.app.post("/remove_order", data=json.dumps(non_existent_entry), headers=self.header)
        assert rv.status_code == 404
        entry = dict(userid=113, quantity=20, price=1.3, type="SELL")
        rv = self.app.post("/remove_order", data=json.dumps(entry), headers=self.header)
        assert rv.status_code == 200

    def test_aggregate_order(self):
        add_data(self.app, self.header)
        rv = self.app.get("/summary_order")
        results = json.loads(rv.data)
        assert results["aggr_buy"] == [u'BUY: 2 kg for \xa32.3']
        assert results["aggr_sell"] == [u'SELL: 2 kg for \xa31.2', u'SELL: 40 kg for \xa31.3']

    def test_empty_db(self):
        rv = self.app.get('/summary_order')
        assert rv.status_code == 200
        resp = json.loads(rv.data)
        assert 'aggr_buy' in resp
        assert 'aggr_sell' in resp

if __name__ == '__main__':
    unittest.main()