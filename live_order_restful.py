#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
live-order-restful.py is an REST API for Silver Live Order Board using Python/flask framework !  
Usage: live-order-restful.py 
                       

"""

from flask import app
import logging
from flask import Flask, request
from flask_restful import Resource, Api, abort
import collections
from collections import OrderedDict


# Reference Docs:
# Flask - Python WebFramework: http://flask.pocoo.org/docs/0.12/
# REST API: http://www.restapitutorial.com/ 

app = Flask(__name__)
api = Api(app)

logger = logging.getLogger('Logger')
logger.setLevel(logging.DEBUG)

Order = collections.namedtuple("Order", 'userid quantity price type')

data = []

# schema: {"userid": 1234, "quantity": 1, "price": 303, "type": "BUY"}
class RegisterOrder(Resource):

    def post(self):
        order_json = request.json

        order = Order(userid=order_json["userid"], quantity=order_json["quantity"],
                      price=order_json["price"], type=order_json["type"])
        data.append(order)
        return {'message': 'OK'}

api.add_resource(RegisterOrder, '/register_order')

# schema: {"userid": 1234, "quantity": 1, "price": 303, "type": "BUY"}
class RemoveOrder(Resource):

    def post(self):
        order_json = request.json
        order = Order(userid=order_json["userid"], quantity=order_json["quantity"],
                      price=order_json["price"], type=order_json["type"])

        removed = False
        for d in data:
            if order == d:
                data.remove(d)
                removed = True

        if removed:
            return {'message': 'OK'}
        else:
            abort(404, message="Order not found")

api.add_resource(RemoveOrder, '/remove_order')

class SummaryOrder(Resource):

    def get(self):
        summary_buy = {}
        summary_sell = {}
        for d in data:
            if d.type == "BUY":
                if d.price not in summary_buy:
                    summary_buy[d.price] = d.quantity
                else:
                    summary_buy[d.price] += d.quantity
            if d.type == "SELL":
                if d.price not in summary_sell:
                    summary_sell[d.price] = d.quantity
                else:
                    summary_sell[d.price] += d.quantity

# Python dictionaries can’t be sorted. Hence convereted into lists to sort them 
        r_buy = ["BUY: " + (str(dd[1]) + " kg for £" + str(dd[0])) for dd in OrderedDict(reversed(sorted(summary_buy.items(), key=lambda x:x[0]))).iteritems()]
        r_sell = ["SELL: " + (str(dd[1]) + " kg for £" + str(dd[0])) for dd in OrderedDict(sorted(summary_sell.items(), key=lambda x:x[0])).iteritems()]

        return {"aggr_buy": r_buy , "aggr_sell": r_sell}


api.add_resource(SummaryOrder, '/summary_order')

# Sample Output
"""
Following is sample output, please use preset live-order-restful.postman_collection (to generate similar data on postman rest client)

http://0.0.0.0:9113/summary_order

{
  "aggr_buy": [
    "BUY: 5.5 kg for £520",
    "BUY: 2.5 kg for £500",
    "BUY: 5 kg for £420",
    "BUY: 2 kg for £303"
  ],
  "aggr_sell": [
    "SELL: 1 kg for £100",
    "SELL: 4.5 kg for £220",
    "SELL: 3.5 kg for £420",
    "SELL: 5 kg for £600",
    "SELL: 15 kg for £920"
  ]
}
"""

if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("9113"),
        threaded=False,
        debug=False)