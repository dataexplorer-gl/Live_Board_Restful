### README ###

Flask-RESTful provides the Framework for creating a great REST API


# Running the API:

## There are two ways of running 

### 1) Using Executable/library <br />
(Prepackaged dependencies - No need of Pre-requisites ) <br />

Access Executables Folder and download the appropriate executable file, currently available in 2 different flavours<br />
* Mac OSX<br />
* Linux <br />

*** Usage instructions are provided under Executables OS directories <br />
*** Executables are packaged using "pyinstaller"<br />

<b>Reference:</b> <br />
PyInstaller Manual - https://pythonhosted.org/PyInstaller/<br />

### 2) Running python API using Python runtime <br />
(Requires dependencies installed)<br />


* Install Requirements: 

```
pip install -r requirements.txt
```

* Execute python rest api 

```
python live_order_restful.py
```

*** Requirements are only required if you running API using (2) Running python API using Python runtime

## Live-Order-Restful API Usage:

* For all POST/GET requests - please use header as "content-type/Accept application/json"

### Register Order:POST Method

(Requires Rest Client, Ex: POSTMAN) <br />

http://0.0.0.0:9113/register_order<br />
Example: {"userid":1234, "quantity":1, "price":303, "type":"BUY"}<br />


### Remove Order:POST Method

(Requires Rest Client, Ex: POSTMAN)<br />

http://0.0.0.0:9113/remove_order<br />
Example: {"userid":1234, "quantity":1, "price":303, "type":"BUY"}<br />


### Summary Order:GET Method

(can be accessed web browser or Rest client)<br />

http://0.0.0.0:9113/summary_order<br />


### Sample Output

"""
Following is sample output, please use preset "live_order_restful.postman_collection" (to generate similar data on postman rest client)<br />

http://0.0.0.0:9113/summary_order<br />

{<br />
  "aggr_buy": [<br />
    "BUY: 5.5 kg for £520",<br />
    "BUY: 2.5 kg for £500",<br />
    "BUY: 5 kg for £420",<br />
    "BUY: 2 kg for £303"<br />
  ],<br />
  "aggr_sell": [<br />
    "SELL: 1 kg for £100",<br />
    "SELL: 4.5 kg for £220",<br />
    "SELL: 3.5 kg for £420",<br />
    "SELL: 5 kg for £600",<br />
    "SELL: 15 kg for £920"<br />
  ]<br />
}<br />
"""