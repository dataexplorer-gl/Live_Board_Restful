# Readme 

## How to Run<br />


1) Download Live_Board_Restful<br />
2) Run using "open" command

<b>Ex:</b> Connect to Terminal <br />

```
open -a live_order_restful_MacOSX.app 
```

Access the Rest API using Rest Client for all Requests (http://0.0.0.0:9113/"resource-type")


## Live-Order-Restful API Usage:

* For all POST/GET requests - please use header as "content-type/Accept application/json"

### Register Order:POST Method

(Requires Rest Client, Ex: POSTMAN) <br />

http://0.0.0.0:9113/register_order<br />
Example: {"userid":1234, "quantity":1, "price":303, "type":"BUY"}<br />


### Remove Order:POST Method

(Requires Rest Client, Ex: POSTMAN)<br />

http://0.0.0.0:9113/remove_order<br />
Example: {"userid":1234, "quantity":1, "price":303, "type":"BUY"}<br />


### Summary Order:GET Method

(can be accessed web browser or Rest client)<br />

http://0.0.0.0:9113/summary_order<br />


### Sample Output

"""
Following is sample output, please use preset "live_order_restful.postman_collection" (to generate similar data on postman rest client)<br />

http://0.0.0.0:9113/summary_order<br />

{<br />
  "aggr_buy": [<br />
    "BUY: 5.5 kg for £520",<br />
    "BUY: 2.5 kg for £500",<br />
    "BUY: 5 kg for £420",<br />
    "BUY: 2 kg for £303"<br />
  ],<br />
  "aggr_sell": [<br />
    "SELL: 1 kg for £100",<br />
    "SELL: 4.5 kg for £220",<br />
    "SELL: 3.5 kg for £420",<br />
    "SELL: 5 kg for £600",<br />
    "SELL: 15 kg for £920"<br />
  ]<br />
}<br />
"""